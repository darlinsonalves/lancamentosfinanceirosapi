using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LancamentosFinanceiro.Models;
using LancamentosFinanceiro.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using LancamentosFinanceiro.Service;
using PusherServer;
using System.Net;
using System.Net.Http;
using System.Web;

namespace LancamentosFinanceiro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class LancamentoController: ControllerBase
    {
        private readonly LancamentoRepository _repository;
        private IBackgroundTaskQueue _queue;
        
        public LancamentoController(LancamentoRepository repository, IBackgroundTaskQueue queue)
        {
            _repository = repository;
            _queue = queue;

            
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Lancamento>>> GetLancamentos()
        {
            return await _repository.Find();
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<Lancamento>> GetLancamento(long id)
        {
            //var Lancemanto = await _repository.GetConta(id);
            //if(Lancemanto == null)
            //{
              //  return NotFound();
            //}
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult<Lancamento>> PostLancamento([FromBody]Lancamento lancamento)
        {
            try
            {                
                return Ok(await _repository.PostLancamento(lancamento));
            } catch(InvalidOperationException e )
            {
                return BadRequest(e.Message);
            } catch(Exception ie) {
                return BadRequest(ie.Message);
            }
        }

        [HttpGet("dashboard")]
        public async Task<ActionResult<Lancamento>> Dashbord()
        {
            var dash = await _repository.GetAlComplete();
            return Ok(dash);
        }

        [HttpPost("import/{conta}/{agencia}")]
        public async Task<ActionResult<IEnumerable<Lancamento>>> PostImport(List<Lancamento> lancamentos, string conta, string agencia)
        {
            var options = new PusherOptions
            {
                Cluster = "mt1",
                Encrypted = true
            };

            var pusher = new Pusher(
            "703656",
            "9d3dec3e7090c3ad1fe5",
            "c60573c26330ad3cc031",
            options);

            


            //_queue
             _queue.QueueBackgroundWorkItem(async token =>
                {
                    var guid = Guid.NewGuid().ToString();
                    /*
                    var result = await pusher.TriggerAsync(
                    "geral-importacao",
                    "importacao",
                    new { message = "sistema iniciou uma importacao de lancamentos!" } );
                    */
                    for (int delayLoop = 0; delayLoop < lancamentos.Count(); delayLoop++)
                    {
                        var lancamento = lancamentos.ElementAt(delayLoop);
                        lancamento.Agencia = agencia;
                        lancamento.Conta = conta;                        
                        await _repository.PostLancamento(lancamentos.ElementAt(delayLoop));
                        Console.WriteLine("importado {0}", lancamentos.ElementAt(delayLoop) );
                        await Task.Delay(TimeSpan.FromSeconds(5), token);
                    }

                    /*
                    result = await pusher.TriggerAsync(
                    "geral-importacao",
                    "importacao",
                    new { message = "arquivo importado!" } );
                     */
                    

                    Console.WriteLine("fim da importação");
                });
            return Ok();
        }
    }
}