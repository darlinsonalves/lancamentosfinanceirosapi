FROM microsoft/dotnet:2.2-sdk
WORKDIR /var/www

COPY  . .
#RUN dotnet add package DnsClient --version 1.0.7
EXPOSE 5000
EXPOSE 5001
#ENV ASPNETCORE_URLS https://*:8080

RUN dotnet publish
RUN dotnet restore

#RUN dotnet add package Microsoft.CodeAnalysis.CSharp.Workspaces --version 2.8.0-dev-62823-01 --source https://dotnet.myget.org/F/dotnet-core/api/v3/index.json

ENTRYPOINT ["dotnet", "./bin/Debug/netcoreapp2.1/LancamentosFinanceiro.dll"]

